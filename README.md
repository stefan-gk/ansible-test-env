### To run: ###
* `cd $HOME && git clone git@bitbucket.org:stefan-gk/ansible-test-env.git && cd ansible-test-env`
* Then, to launch the VM's: `vagrant up`

### Specific: ###
* To run on a different host, edit the NIC variables in `Vagrantfile`.

### To run the playbook: ###
* When the Vagrantfile is up: `vagrant ssh ansible_server_01`
* Then, in the VM: `cd /vagrant && ansible-playbook ansible-playbook.yml -i files/inventories/vagrant-inventory.yml`

### To SSH in from host: ###
* `cd ansible-test-env && ssh -i files/vagrant/vagrant_ansible vagrant@192.168.68.203` or .01 or .02 for the Ansible clients.